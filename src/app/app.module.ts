import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BarChart1Component } from './Component/bar-chart1/bar-chart1.component';            
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BarHorizonComponent } from './Component/bar-horizon/bar-horizon.component';
import { GroupBarChartComponent } from './Component/group-bar-chart/group-bar-chart.component';
import { PieChartComponent } from './Component/pie-chart/pie-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    BarChart1Component,
    BarHorizonComponent,
    GroupBarChartComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule,
    NgxChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class ChartsNg2Module { }